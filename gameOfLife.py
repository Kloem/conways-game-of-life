import pygame
import numpy as np
import random
import time
import argparse
import json
import copy

#parser = argparse.ArgumentParser(description='Init state')
#parser.add_argument('--s', help='define start position:\nr -> random\nt -> toad')
#args = parser.parse_args()

col_about_to_die = (200, 200, 225)
col_alive = (0, 0, 0)
col_background = (10, 10, 40)
col_grid = (30, 30, 60)
dimx = 25
dimy = 25
size_of_cell = 20
rnd = random.Random()
cells = np.zeros((dimy, dimx))
templates = open('gameOfLifeShapes.json')

screen = pygame.display.set_mode([dimx*size_of_cell, dimy*size_of_cell])
screen.fill((255, 255, 255))
pygame.display.set_caption("John Conways Game of Life")

def draw():
    #print("drawing")
    screen.fill([255,255,255])
    for i in range(0,dimy):
        for j in range(0,dimx):
            if(cells[i][j] == 1):
                pygame.draw.rect(screen, col_alive, pygame.draw.rect(screen, col_alive, pygame.Rect(j*size_of_cell, i*size_of_cell, size_of_cell, size_of_cell)))

def cellsAliveAround(x,y, mapOfCells):
    #print("cells around")
    cellsAlive = 0
    for i in range(y-1, y+2):
        for j in range(x-1, x+2):
            if(i>=0 and j >= 0) and (i<dimy and j<dimx):
                if(mapOfCells[i][j]==1 and not (i == y and j == x)):
                    cellsAlive += 1
    return cellsAlive
def update():
    workingMapOfCells = copy.deepcopy(cells)
    for i in range(0,dimy):
        for j in range(0,dimx):
            cellsAround = cellsAliveAround(j, i, mapOfCells=workingMapOfCells)
            if(cellsAround <= 1):
                cells[i][j] = 0
            if(cellsAround == 3):
                cells[i][j] = 1
            if (cellsAround >= 4):
                cells[i][j] = 0
    draw()
    #print("drawn")

def startState(s):
    global cells
    tem = json.load(templates)
    if(s == 'r'):
        for i in range(0,dimy):
            for j in range(0,dimx):
                cells[i][j] = rnd.randint(0,1)
    if (s == 't'):
        cells = tem['toad']
    if(s == 'p'):
        cells = tem['pentomino']
    if (s == 'g'):
        cells = tem['glider']
def main():
    startState('r')
    running = True
    pygame.init()
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        pygame.display.flip()
        time.sleep(0.2)
        update()

    pygame.quit()

main()