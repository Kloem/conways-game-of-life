import pygame
import numpy as np
import time

col_alive = (0, 0, 0)
col_background = (10, 10, 40)
col_grid = (30, 30, 60)
size_of_cell = 20
dimx = 25
dimy = 25

screen = pygame.display.set_mode([dimx*size_of_cell, dimy*size_of_cell])
screen.fill((255, 255, 255))
pygame.display.set_caption("John Conways Game of Life")

def draw():
    screen.fill([255,255,255])
    for i in range(0,dimy):
        for j in range(0,dimx):
            if(cells[i][j] == 1):
                pygame.draw.rect(screen, col_alive, pygame.draw.rect(screen, col_alive, pygame.Rect(j*size_of_cell, i*size_of_cell, size_of_cell, size_of_cell)))


cells = np.zeros((dimy, dimx))

def startState(state):
    #zacetna postavitev celic
    return

def cellsAliveAround(x,y, mapOfCells):
    #stevilo celic okoli izbrane celice
    return cellsAlive

def update():
    #posodobi stanje celic
    return

def main():
    startState('t')
    running = True
    pygame.init()
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        pygame.display.flip()
        time.sleep(0.2)
        update()

    pygame.quit()

main()
